﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public MongoDbInitializer(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;

            _roleRepository = roleRepository;
        }

        public void InitializeDb()
        {
            var roles = _roleRepository.GetAllAsync()
                .GetAwaiter()
                .GetResult();

            if (roles.Count() > 0)
            {
                return;
            }

            foreach (var item in FakeDataFactory.Roles)
            {
                _roleRepository.AddAsync(item);
            }

            foreach (var item in FakeDataFactory.Employees)
            {
                _employeeRepository.AddAsync(item);
            }


        }
    }
}
