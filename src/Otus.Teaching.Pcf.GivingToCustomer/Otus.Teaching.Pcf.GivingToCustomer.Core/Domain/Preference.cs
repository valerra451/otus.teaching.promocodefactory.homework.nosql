﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    [BsonCollection("Preference")]
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}