﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public MongoDbInitializer(IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        public void InitializeDb()
        {
            var preferences = _preferenceRepository.GetAllAsync()
                .GetAwaiter()
                .GetResult();

            if (preferences.Count() > 0)
            {
                return;
            }

            foreach (var item in FakeDataFactory.Preferences)
            {
                _preferenceRepository.AddAsync(item);
            }

            foreach (var item in FakeDataFactory.Customers)
            {
                _customerRepository.AddAsync(item);
            }
        }
    }
}
