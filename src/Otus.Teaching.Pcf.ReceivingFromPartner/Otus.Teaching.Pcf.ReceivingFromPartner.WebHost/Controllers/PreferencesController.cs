﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Cache;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly ICache<PreferenceResponse> _cache;

        public PreferencesController(IRepository<Preference> preferencesRepository,
            ICache<PreferenceResponse> cache)
        {
            _preferencesRepository = preferencesRepository;
            _cache = cache;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var response = await _cache.Get();

            if (response.Any())
            {
                return Ok(response);
            }

            var preferences = await _preferencesRepository.GetAllAsync();

            response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            await _cache.Set(response);

            return Ok(response);
        }
    }
}