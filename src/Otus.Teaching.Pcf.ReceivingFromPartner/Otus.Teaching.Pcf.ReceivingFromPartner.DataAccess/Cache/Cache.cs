﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Cache
{
    public class Cache<T> : ICache<T>
    {
        private readonly IDistributedCache _cache;

        public Cache(IDistributedCache cache)
        {
            _cache = cache;
        }

        private string Key => $"{typeof(T).Name}";

        public async Task<List<T>> Get()
        {
            var cacheValue = await _cache.GetStringAsync(Key);

            return cacheValue is null
               ? new List<T>()
               : JsonConvert.DeserializeObject<List<T>>(cacheValue);
        }

        public async Task Set(IEnumerable<T> value)
        {
            var cacheValue = JsonConvert.SerializeObject(value);

            await _cache.SetStringAsync(Key, cacheValue);
        }
    }
}
