﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Cache
{
    public interface ICache<T>
    {
        Task<List<T>> Get();

        Task Set(IEnumerable<T> value);
    }
}
